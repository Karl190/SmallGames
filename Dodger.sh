#!/bin/bash
command -v convert >/dev/null 2>&1 || { echo >&2 "Dodger requires imagemagick but it's not installed. Aborting."; exit 1; }

rm -rf assets/expanded &&
mkdir -p assets/expanded &&
convert -coalesce "assets/Space.gif" "assets/expanded/Space%02d.png" &&
convert -coalesce "assets/spaceship.gif" "assets/expanded/spaceship%01d.png"
python3 "Dodger.py"